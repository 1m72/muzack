<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsOnSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule', function($table)
        {
            $table->integer('execution_time');
            $table->boolean('finished')->default(false);
            $table->integer('start_time')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule', function($table)
        {
            $table->dropColumn('execution_time');
            $table->dropColumn('finished');
        });
    }
}
