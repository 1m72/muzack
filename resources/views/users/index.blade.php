@extends('cp')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="page-header">
      <a class="btn btn-success btn-sm" type="button" href="<?php echo route('users-create'); ?>">Add new</a>
      <a class="btn btn-default btn-sm hidden_ele" href="#" id="trash">Delete</a>
    </div>
  </div>
  <!-- /.col-lg-12 -->
</div>
<?php echo View::make('partials.messages');?>
<div class="row" id="mainContent">
  <form id="list-form" method="post">
    <div class="col-lg-6">
      <div class="panel panel-default">
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-hover" id="list-items">
              <colgroup>
                <col class="doclist-col-active-indicator">
                <col class="doclist-col-checkbox">
                <col class="doclist-col-star">
                <col class="doclist-col-name">
                <col class="doclist-col-owners">
                <col class="doclist-col-date">
              </colgroup>
              <thead>
                <tr>
                  <th><input type="checkbox" class="massaction-checkbox-all"></th>
                  <th>ID</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>created_at</th>
                  <th>updated_at</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($list as $l){?>
                <tr>
                  <td><input type="checkbox" class="massaction-checkbox" value="<?php echo $l->id; ?>"></td>
                  <td><?php echo $l->id; ?></td>
                  <td><a href="<?php echo route('users-edit', array('id' => $l->id)); ?>"><?php echo $l->email; ?></a></td>
                  <td><?php echo $l->username; ?></td>
                  <td><?php echo $l->created_at; ?></td>
                  <td><?php echo $l->updated_at; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
  </form>
  <!-- 	<div class="aeg"> Some text </div> -->
</div>
<script type="text/javascript">
  templateActions.toIndex();
</script>
<!-- /.row -->
@stop