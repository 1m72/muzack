@extends('cp')
@section('content')
<script type="text/javascript">
var VALIDATE_RULES =
  {
    rules: {
        email: "required",
        password: "required"
    }

  };
</script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Create</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            <form id="create-form" class="form-horizontal" method="post" autocomplete="nope">
              <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-8">
                  <input class="form-control" autocomplete="off" type="text" name="email">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Enter password (This password won't be stored in database)</label>
                <div class="col-lg-8">
                  <input class="form-control" type="password" name="password" autocomplete="off">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Send as</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text" name="username">
                </div>
              </div>

              <!-- /.form-user -->
              <div class="form-group">
                <label class="control-label col-lg-2" for="autosize"></label>
                <div class="col-lg-8">
                  <button type="submit" class="btn btn-success btn-sm" id="btn-submit">Save</button>
                  <a href="<?php echo route('users-index');?>" class="btn btn-primary btn-sm">Back</a>
                </div>
              </div>
              <!-- /.form-user -->
            </form>
          </div>
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<script type="text/javascript">
  userActions.toEdit();
</script>
@stop