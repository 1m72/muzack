<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/icon/favicon.ico" />
    <title><?php echo !empty($site_title) ? $site_title . ' - '. config('marketing.site_title') : config('marketing.site_title'); ?></title>
    <!-- Core CSS - Include with every page -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/bootstrap/font-awesome/css/font-awesome.css"	rel="stylesheet">
    <!-- Page-Level Plugin CSS - Buttons -->
    <!-- <link href="/bootstrap/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">  -->
    <!-- SB Admin CSS - Include with every page -->
    <link href="/bootstrap/css/sb-admin.css" rel="stylesheet">
    <link href="/bootstrap/css/libraries.css" rel="stylesheet">
    <script type="text/javascript" src="/bootstrap/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="/bootstrap/js/jquery.validate.js"></script>
    <script type="text/javascript" src="/bootstrap/js/libraries.js"></script>
    <script src="/bootstrap/js/all.js"></script>
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
      }

      .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin .checkbox {
        font-weight: normal;
      }
      .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
           -moz-box-sizing: border-box;
                box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
      }
      .form-signin .form-control:focus {
        z-index: 2;
      }
      .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      }
      .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      }
    </style>
  </head>
  <body>
    @yield('content')
    <!-- /#wrapper -->
    <!-- Core Scripts - Include with every page -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script	src="/bootstrap/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- Page-Level Plugin Scripts - Buttons -->
    <!-- SB Admin Scripts - Include with every page -->
    <script src="/bootstrap/js/sb-admin.js"></script>
    <!-- Page-Level Demo Scripts - Buttons - Use for reference -->
    <script type="text/javascript">loginActions.run();marketing();</script>
  </body>
</html>