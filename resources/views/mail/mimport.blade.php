@extends('cp')
@section('content')
<script type="text/javascript" src="/plupload/plupload.full.min.js"></script>
<script type="text/javascript">
var VALIDATE_RULES =
  {
    ignore: "",
    rules: {
      email: "required",
      password: "required"
    },
    messages: {
    }
  };
</script>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"><?php echo $site_title; ?></h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="row" id="pageData" data-upload-action="<?php echo route('xhr-upload');?>">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            <form id="create-form" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label col-lg-2">Please enter your email.</label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" name="email" />
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Please enter your email password</label>
                <div class="col-lg-8">
                  <input type="password" class="form-control" name="password" />
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Group</label>
                <div class="col-lg-8">
                  <select class="form-control" name="group_id">
                    <?php foreach($groups as $group){?>
                      <option value="<?php echo $group->id; ?>"><?php echo $group->title; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <!-- /.form-group -->
              <div class="form-group">
                <label class="control-label col-lg-2" for="autosize"></label>
                <div class="col-lg-8">
                  <button type="submit" class="btn btn-success btn-sm" id="btn-submit">Save</button>
                  <a href="<?php echo route('mail-index');?>" class="btn btn-primary btn-sm">Back</a>
                </div>
              </div>
              <!-- /.form-group -->
            </form>
          </div>
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<script type="text/javascript">mailActions.toEdit();</script>
@stop