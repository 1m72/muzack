@extends('cp')
@section('content')
<script type="text/javascript">
  var VALIDATE_RULES = {
    rules: {
      email: "required",
      password: "required",
      template_id: "required"
    }
  };
</script>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Import</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            <form id="create-form" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label col-lg-2">Email</label>
                <div class="col-lg-8">
                  <input type="text" name="email">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Password</label>
                <div class="col-lg-8">
                  <input type="password" name="password">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Select template</label>
                <div class="col-lg-8">
                  <select name="template_id">
                    <?php foreach($templates as $template){?>
                      <option value="<?php echo $template->id; ?>"><?php echo $template->title; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label class="control-label col-lg-2" for="autosize"></label>
                <div class="col-lg-8">
                  <button type="submit" class="btn btn-success btn-sm" id="btn-submit">Send</button>
                  <a href="<?php echo route('mail-index');?>" class="btn btn-primary btn-sm">Back</a>
                </div>
              </div>
              <!-- /.form-group -->
            </form>
          </div>
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
@stop