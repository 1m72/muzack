@extends('cp')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="page-header clearfix">
      <div class="page-header-actions">
        <a class="btn btn-success btn-sm" href="<?php echo route('mail-himport');?>">Add</a>
        <a class="btn btn-primary btn-sm" href="<?php echo route('mail-import');?>">Import from file</a>
        <a class="btn btn-primary btn-sm" href="<?php echo route('mail-mimport');?>">Import from mailbox</a>
        <a class="btn btn-default btn-sm hidden_ele" href="#" id="trash">Delete</a>
        <a class="btn btn-default btn-sm hidden_ele" href="#" id="toGroup">Add to</a>
      </div>
      <?php echo View::make('partials.pagination')->with(array('list' => $list));?>
    </div>
    <div class="page-header clearfix">
      <div class="page-header-actions">
        <select class="form-control" id="groupFilter">
          <option value="0">All</option>
          <?php foreach($groups as $group){?>
            <option <?php if(isset($filter['group_id']) && $group->id == $filter['group_id']){?>selected<?php } ?> value="<?php echo $group->id?>"><?php echo $group->title; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
  <!-- /.col-lg-12 -->
</div>
<?php echo View::make('partials.messages');?>
<div class="row" id="mainContent" page="mail-index">
  <form id="list-form" method="post">
    <div class="col-lg-6">
      <div class="panel panel-default">
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-hover" id="list-items">
              <colgroup>
                <col class="doclist-col-active-indicator">
                <col class="doclist-col-checkbox">
                <col class="doclist-col-star">
                <col class="doclist-col-name">
                <col class="doclist-col-owners">
                <col class="doclist-col-date">
              </colgroup>
              <thead>
                <tr>
                  <th><input type="checkbox" class="massaction-checkbox-all"></th>
                  <th>ID</th>
                  <th>Address</th>
                  <th>created_at</th>
                  <th>updated_at</th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($filter['group_id'])){
                foreach($list as $email){
                  // _debug($group->emails());
                  // foreach($group as $email){
                  ?>
                <tr>
                <td><input type="checkbox" class="massaction-checkbox" value="<?php echo $email->id; ?>"></td>
                <td><?php echo $email->id; ?></td>
                <td><?php echo $email->address; ?></td>
                <td><?php echo $email->created_at; ?></td>
                <td><?php echo $email->updated_at; ?></td>
                </tr>
                <?php } }else{
                foreach($list as $email){?>
                <tr>
                <td><input type="checkbox" class="massaction-checkbox" value="<?php echo $email->id; ?>"></td>
                <td><?php echo $email->id; ?></td>
                <td><?php echo $email->address; ?></td>
                <td><?php echo $email->created_at; ?></td>
                <td><?php echo $email->updated_at; ?></td>
                </tr>
                <?php }
                }?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
    <input type="hidden" name="filter" id="filterInput" value='<?php echo json_encode($filter); ?>'>
  </form>
  <!-- 	<div class="aeg"> Some text </div> -->
</div>
<script>mailActions.toIndex();</script>
<!-- /.row -->
@stop