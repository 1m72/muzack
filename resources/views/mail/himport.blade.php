@extends('cp')
@section('content')
<script type="text/javascript">
var VALIDATE_RULES =
  {
    rules: {
        address: "required"
    }
  };
</script>
<div class="row">
  <div class="col-lg-12">
    <div class="page-header clearfix">
      <div class="page-header-actions">
        <a class="btn btn-default btn-sm" href="#" id="newGroup">New group</a>
      </div>
    </div>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
    <div class="panel-heading">Add new email</div>
      <div class="panel-body" id="mainContent">
        <div class="row">
          <div class="col-lg-6">
            <form id="create-form" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label col-lg-2">Address</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text" name="address" />
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Group</label>
                <div class="col-lg-8">
                  <select class="form-control" name="group_id">
                    <?php foreach($groups as $group){?>
                      <option value="<?php echo $group->id; ?>"><?php echo $group->title; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <!-- /.form-group -->
              <div class="form-group">
                <label class="control-label col-lg-2"></label>
                <div class="col-lg-8">
                  <button type="submit" class="btn btn-success btn-sm" id="btn-submit">Save</button>
                  <a href="<?php echo route('mail-index');?>" class="btn btn-primary btn-sm">Back</a>
                </div>
              </div>
              <!-- /.form-group -->
            </form>
          </div>
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<script type="text/javascript">mailActions.toEdit(); mailActions.tohImport();</script>
@stop