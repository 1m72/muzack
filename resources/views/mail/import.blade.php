@extends('cp')
@section('content')
<script type="text/javascript" src="/plupload/plupload.full.min.js"></script>
<script type="text/javascript">
var VALIDATE_RULES =
  {
    ignore: "",
    rules: {
      upload_id: "required"
    },
    messages: {
      upload_id: ""
    }
  };
</script>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Import</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="row" id="pageData" data-upload-action="<?php echo route('xhr-upload');?>">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            <form id="create-form" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label col-lg-2">Please choose file (.txt)</label>
                <div class="col-lg-8">
                  <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
                    <br />
                    <div id="container">
                        <a id="pickfiles" href="javascript:;">[Select files]</a>
                        <a id="uploadfiles" href="javascript:;">[Upload files]</a>
                    </div>
                    <input type="hidden" name="upload_id" id="upload_id" />
                    <br />
                    <pre id="upload-error"></pre>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Group</label>
                <div class="col-lg-8">
                  <select class="form-control" name="group_id">
                    <?php foreach($groups as $group){?>
                      <option value="<?php echo $group->id; ?>"><?php echo $group->title; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <!-- /.form-group -->
              <div class="form-group">
                <label class="control-label col-lg-2" for="autosize"></label>
                <div class="col-lg-8">
                  <button type="submit" class="btn btn-success btn-sm" id="btn-submit">Save</button>
                  <a href="<?php echo route('mail-index');?>" class="btn btn-primary btn-sm">Back</a>
                </div>
              </div>
              <!-- /.form-group -->
            </form>
          </div>
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<script type="text/javascript">mailActions.toEdit(); mailActions.toImport();</script>
@stop