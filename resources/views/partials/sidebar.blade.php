        <div class="navbar-default navbar-static-side" role="navigation">
          <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
              <li class="sidebar-search">
                <div class="input-group custom-search-form">
                  <input type="text" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                  </button>
                  </span>
                </div>
                <!-- /input-group -->
              </li>
              <li>
                <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Mail Templates<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="<?php echo route('templates-index'); ?>">List</a>
                  </li>
                  <li>
                    <a href="<?php echo route('templates-edit'); ?>">New new</a>
                  </li>
                </ul>
                <!-- /.nav-second-level -->
              </li>
              <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Email Management<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="<?php echo route('mail-index'); ?>">List</a>
                  </li>
                  <li>
                    <a href="<?php echo route('mail-import'); ?>">Import</a>
                  </li>
                </ul>
                <!-- /.nav-second-level -->
              </li>
              <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Schedule<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="<?php echo route('schedule-index'); ?>">List</a>
                  </li>
                  <li>
                    <a href="<?php echo route('schedule-edit'); ?>">New</a>
                  </li>
                </ul>
                <!-- /.nav-second-level -->
              </li>
              <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Groups<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="<?php echo route('groups-index'); ?>">List</a>
                  </li>
                  <li>
                    <a href="<?php echo route('groups-edit'); ?>">New</a>
                  </li>
                </ul>
                <!-- /.nav-second-level -->
              </li>

              <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Users<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="<?php echo route('users-index'); ?>">List</a>
                  </li>
                  <li>
                    <a href="<?php echo route('users-edit'); ?>">New</a>
                  </li>
                </ul>
                <!-- /.nav-second-level -->
              </li>
            </ul>
            <!-- /#side-menu -->
          </div>
          <!-- /.sidebar-collapse -->
        </div>
