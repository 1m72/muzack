<div class="row">
  <div class="col-lg-6">
    <?php
      $msg = array('success', 'danger');
      foreach($msg as $m){
        if(Session::has('msg-'.$m)){?>
          <div class="alert alert-<?php echo $m; ?>"><?php echo Session::get('msg-'.$m); ?></div>
          <?php
        }
      }?>
  </div>
</div>