<?php if(isset($list) && ($list instanceof Illuminate\Pagination\LengthAwarePaginator)){?>
  <div class="page-header-pagination">
    <?php echo $list->appends(Input::except('page'))->render(); ?>
  </div>
<?php } ?>