@extends('cp')
@section('content')
<script type="text/javascript">
var VALIDATE_RULES =
  {
    rules: {
        title: "required"
    }

  };
</script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Create</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            <form id="create-form" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label col-lg-2">Group name</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text" name="title" value="<?php echo $group->title; ?>">
                </div>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label class="control-label col-lg-2" for="autosize"></label>
                <div class="col-lg-8">
                  <button type="submit" class="btn btn-success btn-sm" id="btn-submit">Save</button>
                  <a href="<?php echo route('groups-index');?>" class="btn btn-primary btn-sm">Back</a>
                </div>
              </div>
              <!-- /.form-group -->
            </form>
          </div>
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<script type="text/javascript">
  groupActions.toEdit();
</script>
@stop