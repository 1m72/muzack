<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/icon/favicon.ico" />
    <title><?php echo !empty($site_title) ? $site_title . ' - '. config('marketing.site_title') : config('marketing.site_title'); ?></title>
    <!-- Core CSS - Include with every page -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/bootstrap/font-awesome/css/font-awesome.css"	rel="stylesheet">
    <!-- Page-Level Plugin CSS - Buttons -->
    <!-- <link href="/bootstrap/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">  -->
    <!-- SB Admin CSS - Include with every page -->
    <link href="/bootstrap/css/sb-admin.css" rel="stylesheet">
    <link href="/bootstrap/css/libraries.css" rel="stylesheet">
    <script type="text/javascript" src="/bootstrap/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="/bootstrap/js/jquery.validate.js"></script>
    <script type="text/javascript" src="/bootstrap/js/libraries.js"></script>
    <!-- Core Scripts - Include with every page -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script	src="/bootstrap/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <!-- Page-Level Plugin Scripts - Buttons -->
    <!-- SB Admin Scripts - Include with every page -->
    <script src="/bootstrap/js/sb-admin.js"></script>
    <!-- Page-Level Demo Scripts - Buttons - Use for reference -->
    <script src="/bootstrap/js/all.js"></script>
  </head>
  <body>
    <div class="vI8oZc cS">
      <div class="wl"></div>
      <div class="wq"></div>
      <div class="wp"></div>
      <div class="wo"></div>
      <div class="wn"></div>
    </div>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">NguyenThanh-PC</a>
        </div>
        <!-- /.navbar-header -->
        <?php echo View::make('partials/header_actions'); ?>
        <!-- /.navbar-top-links -->
        <?php echo View::make('partials/sidebar'); ?>
        <!-- /.navbar-static-side -->
      </nav>
      <div id="page-wrapper">
        @yield('content')
      </div>
      <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <script type="text/javascript">marketing();</script>
  </body>
</html>