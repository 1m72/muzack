@extends('cp')
@section('content')
<script type="text/javascript">
var VALIDATE_RULES =
  {
    rules: {
        title: "required",
        start_time: "required"
    }
  };
</script>
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Add</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<?php if($schedule->finished){?>
<div class="row">
  <div class="col-lg-6">
    <div class="alert alert-info">This task is completed.</div>
  </div>
</div>
<?php } ?>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-6">
            <form id="create-form" class="form-horizontal" method="post" enctype="multipart/form-data">

              <div class="form-group">
                <label class="control-label col-lg-2">Title</label>
                <div class="col-lg-8">
                  <input class="form-control" type="text" <?php if($schedule->finished){ ?>disabled<?php } ?> name="title" value="<?php echo $schedule->title; ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Group</label>
                <div class="col-lg-8">
                  <select class="form-control" id="group_id" data-default="<?php echo $schedule->group_id; ?>" name="group_id" <?php if($schedule->finished){ ?>disabled<?php } ?>>
                    <?php foreach($groups as $group){?>
                      <option value="<?php echo $group->id; ?>" <?php if($group->id == $schedule->group_id){ echo 'selected'; } ?>><?php echo $group->title; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Choose template</label>
                <div class="col-lg-8">
                  <select class="form-control" name="template_id" <?php if($schedule->finished){ ?>disabled<?php } ?>>
                    <?php foreach($templates as $template){?>
                      <option value="<?php echo $template->id; ?>" <?php if($template->id == $schedule->template_id){ echo 'selected'; } ?>><?php echo $template->title; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-lg-2">Choose sender</label>
                <div class="col-lg-8">
                  <select class="form-control" name="user_id" <?php if($schedule->finished){ ?>disabled<?php } ?>>
                    <?php foreach($users as $user){?>
                      <option value="<?php echo $user->id; ?>" <?php if($user->id == $schedule->user_id){ echo 'selected'; } ?>><?php echo $user->email; ?> <?php if($user->username){echo '('.$user->username.')'; }?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

               <div class="form-group">
                <label class="control-label col-lg-2">Choose schedule</label>
                <div class="col-lg-8">
                <div class="input-group date" id="start_time">
                    <input type="text" <?php if($schedule->finished){ ?>disabled<?php } ?> class="form-control" name="start_time" value="<?php echo iif(isset($schedule->start_time), date('Y-m-d H:i', $schedule->start_time)); ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                </div>
              </div>
              <!-- /.form-group -->
              <div class="form-group">
                <label class="control-label col-lg-2" for="autosize"></label>
                <div class="col-lg-8">
                  <?php if(!$schedule->finished){?>
                    <button type="submit" class="btn btn-success btn-sm" id="btn-submit">Save</button>
                  <?php } ?>
                  <a href="<?php echo route('schedule-index');?>" class="btn btn-primary btn-sm">Back</a>
                </div>
              </div>
              <!-- /.form-group -->
            </form>
          </div>
        </div>
        <!-- /.row (nested) -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<script type="text/javascript">scheduleActions.toEdit();</script>
@stop