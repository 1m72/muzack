@extends('cp')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="page-header clearfix">
      <div class="page-header-actions">
        <a class="btn btn-success btn-sm" type="button" href="<?php echo route('schedule-edit'); ?>">Add new</a>
        <a class="btn btn-default btn-sm hidden_ele" href="#" id="trash">Delete</a>
      </div>
      <?php echo View::make('partials.pagination')->with(array('list' => $list));?>
    </div>
  </div>
  <!-- /.col-lg-12 -->
</div>
<?php echo View::make('partials.messages');?>
<div class="row" id="mainContent">
  <form id="list-form" method="post">
    <div class="col-lg-6">
      <div class="panel panel-default">
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-hover" id="list-items">
              <colgroup>
                <col class="doclist-col-active-indicator">
                <col class="doclist-col-checkbox">
                <col class="doclist-col-star">
                <col class="doclist-col-name">
                <col class="doclist-col-owners">
                <col class="doclist-col-date">
              </colgroup>
              <thead>
                <tr>
                  <th><input type="checkbox" class="massaction-checkbox-all"></th>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Start Time</th>
                  <th>Finish</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($list as $l){?>
                <tr>
                  <td><input type="checkbox" class="massaction-checkbox" value="<?php echo $l->id; ?>"></td>
                  <td><?php echo $l->id; ?></td>
                  <td><a href="<?php echo route('schedule-edit', array('id' => $l->id)); ?>"><?php echo $l->title; ?></a></td>
                  <td><?php echo date('Y-m-d H:i:s', $l->start_time); ?></td>
                  <td><?php echo ($l->finished) ? 'Yes' : 'No'; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->
  </form>
  <!-- 	<div class="aeg"> Some text </div> -->
</div>
<script type="text/javascript">
  scheduleActions.toIndex();
</script>
<!-- /.row -->
@stop