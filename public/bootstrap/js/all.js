(function(l) {
  function n(b, e) {
    var f = b.split("."),
      d = l;
    f[0] in d || !d.execScript || d.execScript("var " + f[0]);
    for (var g; f.length && (g = f.shift());) {
      f.length || void 0 === e ? d = d[g] ? d[g] : d[g] = {} : d[g] = e;
    }
  }

  function G(b, e, f) {
    return b.call.apply(b.bind, arguments);
  }

  function u(b, e, f) {
    if (!b) {
      throw Error();
    }
    if (2 < arguments.length) {
      var d = Array.prototype.slice.call(arguments, 2);
      return function() {
        var f = Array.prototype.slice.call(arguments);
        Array.prototype.unshift.apply(f, d);
        return b.apply(e, f);
      };
    }
    return function() {
      return b.apply(e, arguments);
    };
  }

  function t(b, e, f) {
    t = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? G : u;
    return t.apply(null, arguments);
  }

  function p(b, e, f) {
    b = jQuery(document.createElement(b));
    e && b.attr(e);
    f && b.addClass(f);
    return b;
  }

  function H(b, e) {
    e = e && e.toLowerCase();
    for (var f = "", d = 0, g = "a" == e ? 10 : 0, h = "n" == e ? 10 : 62; d++ < b;) {
      var k = Math.random() * (h - g) + g << 0,
        f = f + String.fromCharCode(k + (9 < k ? 36 > k ? 55 : 61 : 48))
    }
    return f;
  }

  function q(b, e, f, d) {
    return jQuery(document.createElement(b)).attr(e).addClass(f).html(d);
  }

  function B(b, e, f) {
    this.returnValue = null;
    return this.run(b, e, f);
  }

  function v(b, e, f, d) {
    this.id = "n" + I();
    this.dlclass = "modal-dialog";
    this.duration = 150;
    this.buttons = f || {};
    this.closeButton = !0;
    this.modal = void 0;
    this.title = b || "Title";
    this.overlay = void 0;
    this.content = e || void 0;
    this.title_parent = null;
    this.buttons_parent = void 0;
    this.loading = d || !1;
    this.w = this.p = this.A = void 0;
    this.renderCallback = null;
  }

  function w() {
    this.uri = void 0;
    this.method = "POST";
    this.data = {};
    this.crossDomain = !1;
    this.dataType = "html";
    this.handler = this.beforeSend = this.errorHandler = this.finallyHandler = void 0;
  }

  function x() {}

  function m() {}

  function y() {}

  function z() {}

  function C() {}

  function D() {}

  function A() {}

  function E() {
    return this.run();
  }
  var d, r = function(b, e) {
    function f() {}
    f.prototype = e.prototype;
    b.prototype = new f;
    b.prototype.constructor = b;
    b.superclass = e;
    b.superproto = e.prototype;
  };
  Function.prototype.bind || (Function.prototype.bind = function(b) {
    function e() {
      return g.apply(this instanceof f && b ? this : b, d.concat(Array.prototype.slice.call(arguments)));
    }

    function f() {}
    if ("function" != typeof this) {
      throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
    }
    var d = Array.prototype.slice.call(arguments, 1),
      g = this;
    f.prototype = this.prototype;
    e.prototype = new f;
    return e;
  });
  B.prototype.run = function(b, e, f) {
    var d = [];
    e && (e = q("div", {}, null, q("span", {}, null, e)), d.push(e));
    b = b || "";
    d.push(q("br", {}, null, null));
    var g = q("input", {
      type: "text",
      id: ":" + H(2)
    }, "form-control");
    e = q("div", {}, null, g);
    d.push(e);
    b && g.val(b);
    this.returnValue = b;
    var h = (new v(h, d, {
      ok: {
        className: "btn btn-success btn-sm",
        label: "OK",
        init: function(b) {
          g.on("keyup", function() {
            0 < this.value.length ? b.prop("disabled", !1) : b.prop("disabled", !0);
          });
        },
        callback: f
      },
      cancel: {
        className: "btn btn-default btn-sm",
        label: "Cancel"
      }
    }, !1)).afterRender(function() {
      g.select();
    });
    this.input = g;
    h.render();
    return this;
  };
  var I = function() {
    var b = 0;
    return function() {
      return b++;
    };
  }();
  d = v.prototype;
  d.setLoading = function(b) {
    this.loading = b;
    return this;
  };
  d.setDialogClass = function(b) {
    this.dlclass = b;
    return this;
  };
  d.setDuration = function(b) {
    this.duration = b;
    return this;
  };
  d.showCloseButton = function(b) {
    this.closeButton = b;
    return this;
  };
  d.setContent = function(b) {
    this.content = b;
    return this;
  };
  d.setTitle = function(b) {
    this.title = b;
    return this;
  };
  d.setButtons = function(b) {
    this.buttons = b;
    return this;
  };
  d.getButtons = function(b) {
    return void 0 == b ? this.buttons : this.buttons[b];
  };
  d.afterRender = function(b) {
    this.renderCallback = b;
    return this;
  };
  d.close = function() {
    var b = this;
    b.modal.animate({
      opacity: 0
    }, b.duration, function() {
      b.modal.remove();
    });
    b.overlay.animate({
      opacity: 0
    }, b.duration, function() {
      b.overlay.remove();
    });
    jQuery(l).off("resize.dialog");
  };
  d.build = function() {
    var b = this;
    this.modal = jQuery(document.createElement("div")).addClass(this.dlclass);
    this.overlay = jQuery(document.createElement("div")).addClass(this.dlclass + "-bg").css({
      width: jQuery(l).width() + "px",
      height: jQuery(l).height() + "px",
      opacity: 0
    }).animate({
      opacity: .75
    }, this.duration);
    this.title_parent = jQuery(document.createElement("div")).addClass(this.dlclass + "-title " + this.dlclass + "-title-draggable");
    this.title_text = jQuery(document.createElement("div")).addClass(this.dlclass + "-title-text");
    this.dialog_content = jQuery(document.createElement("div")).addClass(this.dlclass + "-content");
    if (this.title_text.html(this.title), this.dialog_content.html(this.content), this.closeButton && (this.closeButton = jQuery(document.createElement("div")).addClass(this.dlclass + "-title-close"), this.closeButton.bind("click", function() {
        b.close();
      }), this.closeButton.appendTo(this.title_parent)), Object.keys(this.buttons).length) {
      var e = !1;
      this.buttons_parent = jQuery(document.createElement("div")).addClass(this.dlclass + "-buttons");
      for (a in this.buttons) {
        (function(d, F, g) {
          "cancel" == g && (e = !0);
          var h = p("button");
          h.attr({
            name: g
          });
          h.addClass(d.className || "btn btn-success btn-sm");
          "function" == typeof d.init && d.init(h, g);
          d.callback && h.one("click", function(e) {
            h.addClass("disabled").html("Please wait");
            d.callback.call(this, b);
          });
          d.label && h.html(d.label);
          "cancel" == g && h.one("click", function() {
            b.close();
          });
          h.appendTo(F);
        })(this.buttons[a], this.buttons_parent, a);
      }
      e || (c = p("button").addClass("btn btn-default btn-sm").one("click", function() {
        b.close();
      }).text("Cancel").appendTo(this.buttons_parent));
      this.buttons_parent.appendTo(this.modal);
    }
  };
  d.render = function() {
    this.build();
    this.dialog_content.prependTo(this.modal);
    this.title_text.appendTo(this.title_parent);
    this.title_parent.prependTo(this.modal);
    this.loading || this.dialog_content.css({
      width: "430px"
    });
    this.overlay.appendTo(document.body);
    this.modal.appendTo(document.body).css({
      top: (jQuery(l).height() - this.modal.outerHeight()) / 2 + jQuery(l).scrollTop() + "px",
      left: (jQuery(l).width() - this.modal.outerWidth()) / 2 + jQuery(l).scrollLeft() + "px",
      opacity: 0
    }).animate({
      opacity: 1
    }, this.duration);
    this.loading = !0;
    this.renderCallback && this.renderCallback.call(this);
    jQuery(l).on("resize.dialog", function() {
      this.M();
    }.bind(this));
    console.log(this);
  };
  d.M = function() {
    this.modal.css({
      top: (jQuery(l).height() - this.modal.outerHeight()) / 2 + jQuery(l).scrollTop() + "px",
      left: (jQuery(l).width() - this.modal.outerWidth()) / 2 + jQuery(l).scrollLeft() + "px"
    });
  };
  d = w.prototype;
  d.setUri = function(b) {
    this.uri = b;
    return this;
  };
  d.setMethod = function(b) {
    this.method = b;
    return this;
  };
  d.setData = function(b) {
    this.data = b;
    return this;
  };
  d.setHandler = function(b) {
    this.handler = b;
    return this;
  };
  d.setBeforeSend = function(b) {
    this.beforeSend = b;
    return this;
  };
  d.setErrorHandler = function(b) {
    this.errorHandler = b;
    return this;
  };
  d.setFinallyHandler = function(b) {
    this.finallyHandler = b;
    return this;
  };
  d.send = function() {
    jQuery.ajax({
      url: this.uri,
      type: this.method,
      crossDomain: this.crossDomain,
      beforeSend: function() {
        this.beforeSend && this.beforeSend.call(this);
      }.bind(this),
      data: this.data,
      dataType: this.dataType,
      success: function(b, e, d) {
        this.handler && this.handler.apply(this, arguments);
      }.bind(this),
      error: function() {
        this.errorHandler && this.errorHandler.call(this, arguments);
      }.bind(this),
      complete: function() {
        this.finallyHandler && this.finallyHandler.call(this, arguments);
      }.bind(this)
    });
  };
  d = x.prototype;
  d.checkAll = function(b) {
    b = jQuery(b.currentTarget);
    b.is(":checked") ? jQuery(".massaction-checkbox").prop("checked", !0).parents("tr").addClass("tr-selected") : jQuery(".massaction-checkbox").prop("checked", !1).parents("tr").removeClass("tr-selected");
    this.showHideRemoveButton();
  };
  d.showHideRemoveButton = function() {
    jQuery(".massaction-checkbox:checked").length ? ("mail-index" == jQuery("#mainContent").attr("page") && jQuery("#toGroup").removeClass("hidden_ele"), jQuery("#trash").removeClass("hidden_ele")) : ("mail-index" == jQuery("#mainContent").attr("page") && jQuery("#toGroup").addClass("hidden_ele"), jQuery("#trash").addClass("hidden_ele"));
  };
  d.massactionCheckbox = function(b) {
    var e = jQuery(b.currentTarget);
    if ("A" == b.target.tagName) {
      return l.location.href = e.attr("href"), !1;
    }
    e.toggleClass("tr-selected");
    "checkbox" != b.target.type && jQuery(":checkbox", e).prop("checked", function() {
      return !this.checked;
    });
    jQuery(".massaction-checkbox").length == jQuery(".massaction-checkbox:checked").length ? jQuery(".massaction-checkbox-all").prop("checked", !0) : jQuery(".massaction-checkbox-all").prop("checked", !1);
    this.showHideRemoveButton();
  };
  d.mainContent = function(b) {
    b.css("height", l.innerHeight - b.offset().top - 30 + "px");
    return this;
  };
  d = m.prototype;
  d.toIndex = function() {
    this.trash();
  };
  d.form = function() {
    var b = jQuery("#create-form");
    if (b.length) {
      var e = jQuery.extend({}, {
        messages: {},
        errorElement: "span",
        errorClass: "form-error",
        errorElementClass: "errormsg",
        focusCleanup: !0,
        focusInvalid: !1,
        onkeyup: !1,
        submitHandler: function(b) {
          this.valid() && (l.submitted = !0, jQuery("#btn-submit").html("Saving...").prop("disabled", !0), b.submit());
        }
      }, VALIDATE_RULES);
      b.validate(e);
    }
    return this;
  };
  d.toEdit = function() {
    this.form();
  };
  d.trash = function() {
    var b = this.actions = new x,
      e = jQuery("#trash");
    e.length && (e.on("click", function(b) {
      b.preventDefault();
      if (b = jQuery(".massaction-checkbox:checked").length) {
        for (var e = p("input", {
            type: "hidden",
            value: "",
            name: "ids"
          }).appendTo("#list-form"), d = [], h = 0; h < b; ++h) {
          var k = jQuery(".massaction-checkbox:checked")[h];
          d.push(jQuery(k).attr("value"));
        }
        d = d.join(",");
        e.attr("value", d);
        jQuery("#list-form").submit();
      }
    }), jQuery(document.body).on("click", ".massaction-checkbox, .massaction-checkbox-all", function() {
      0 < jQuery(".massaction-checkbox:checked").length ? e.removeClass("hidden") : jQuery("#delete_all").addClass("hidden");
    }), jQuery("#list-items").find("tbody > tr").hover(function() {
      jQuery(this).addClass("tr-hover");
    }, function() {
      jQuery(this).removeClass("tr-hover");
    }), jQuery(".massaction-checkbox-all").on("click", t(b.checkAll, b)), jQuery("#list-items").find("tr, a").on("click", t(b.massactionCheckbox, b)));
    return this;
  };
  r(y, m);
  d = y.prototype;
  d.getGroups = function(b) {
    if (this.data.groups) {
      return this.data.groups;
    }
  };
  d.toIndex = function() {
    m.prototype.toIndex.call(this);
    jQuery(document).on("click", "#toGroup", function(b) {
      b.preventDefault();
      var d, f;
      jQuery.ajax({
        type: "POST",
        url: "/cp/xhr/groups?action=get",
        success: function(b) {
          d = p("select", {}, "form-control");
          em = p("div");
          for (var g = 0, h = b.length; g < h; g++) {
            0 == g && (f = b[g].id), p("option").val(b[g].id).html(b[g].title).appendTo(d);
          }
          d.on("change", function() {
            f = jQuery(this).val();
          });
          b = jQuery(".massaction-checkbox:checked");
          for (var k = [], g = 0, h = b.length; g < h; ++g) {
            k.push(jQuery(b[g]).attr("value"));
          }
          b = p("div").text("Please select a group:");
          em.append(b, "</br>", d);
          (new v).setTitle("Add email to group").setContent(em).setButtons([{
            label: "OK",
            className: "btn btn-success btn-sm",
            callback: function(b) {
              (new w).setMethod("POST").setData({
                group_id: f,
                emails: k
              }).setUri("/cp/xhr/email?action=to_group").setHandler(function() {
                b.close();
              }).send();
            }
          }]).render();
        }
      });
    });
    jQuery("#groupFilter").on("change", function() {
      var b = {
          group_id: jQuery(this).val(),
          page: 1
        },
        d = jQuery("#filterInput"),
        f = JSON.parse(d.val()),
        b = jQuery.extend({}, f, b);
      d.val(JSON.stringify(b));
      p("input", {
        type: "hidden",
        name: "action"
      }).val("true").appendTo("#list-form");
      jQuery("#list-form").submit();
    });
  };
  d.tohImport = function() {
    jQuery(document.body).on("click", "#newGroup", function(b) {
      b.preventDefault();
      var d;
      b = new B("New group", "Please enter new group title:", function(b) {
        (new w).setData({
          title: d
        }).setUri("/cp/xhr/groups?action=new").setMethod("POST").setHandler(function(d) {
          b.close();
        }).send();
      });
      d = b.returnValue;
      b.input.on("keyup", function() {
        d = this.value;
      });
    });
  };
  d.toImport = function() {
    var b = new plupload.Uploader({
      multi_selection: !1,
      runtimes: "html5,flash,silverlight,html4",
      browse_button: "pickfiles",
      container: document.getElementById("container"),
      url: jQuery("#pageData").attr("data-upload-action"),
      flash_swf_url: "/plupload/js/Moxie.swf",
      silverlight_xap_url: "/plupload/js/Moxie.xap",
      init: {
        PostInit: function() {
          document.getElementById("filelist").innerHTML = "";
          document.getElementById("uploadfiles").onclick = function() {
            b.start();
            return !1;
          };
        },
        FilesAdded: function(b, d) {
          var m = !0;
          jQuery.each(d, function(d, f) {
            var k;
            var l = f.name;
            k = ["txt", "csv"];
            k instanceof String && (k = [k]);
            0 < l.length ? (l = l.substr(l.lastIndexOf(".") + 1).toLowerCase(), k = -1 !== k.indexOf(l) ? !0 : !1) : k = !1;
            k || (m = !1, b.removeFile(f));
          });
          if (!m && 1 >= d.length) {
            return b.splice(), b.refresh(), jQuery("#upload-error").html("File is not allowed."), !1;
          }
          plupload.each(d, function(b) {
            jQuery(document.getElementById("filelist")).append('<div id="' + b.id + '">' + b.name + " (" + plupload.formatSize(b.size) + ") <b></b></div>");
          });
          jQuery("#upload-error").html(null);
        },
        UploadProgress: function(b, d) {
          document.getElementById(d.id).getElementsByTagName("b")[0].innerHTML = "<span>" + d.percent + "%</span>";
        },
        Error: function(b, d) {
          document.getElementById("console").innerHTML += "\nError #" + d.code + ": " + d.message;
        },
        FileUploaded: function(b, d, l) {
          b = JSON.parse(l.response);
          jQuery("#upload_id").attr("value", b.file);
        }
      }
    });
    b.init();
  };
  r(z, m);
  d = z.prototype;
  d.toEdit = function() {
    m.prototype.toEdit.call(this);
    this.datePicker();
    this.groupSelection();
  };
  d.datePicker = function() {
    jQuery("#start_time").datetimepicker({
      format: "YYYY-MM-DD HH:mm"
    });
  };
  d.groupSelection = function() {
    return this;
    // return new loadGroup("#group_id");
  };
  r(C, m);
  r(D, m);
  r(A, m);
  d = A.prototype;
  d.toEdit = function() {
    m.prototype.toEdit.call(this);
    CKEDITOR.replace("content", {
      filebrowserBrowseUrl: "/ckfinder/ckfinder.html",
      filebrowserUploadUrl: "/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files",
      height: "500px"
    });
    var b = jQuery("#create-form");
    CKEDITOR.instances.content.on("change", function() {
      CKEDITOR.instances.content.updateElement();
    });
    b.data("serialize", b.serialize());
    jQuery(l).on("beforeunload", function(d) {
      if (b.serialize() != b.data("serialize") && !l.submitted) {
        return "Are you sure you want to navigate away from this page?";
      }
    });
  };
  d = E.prototype;
  d.run = function() {
    var b = jQuery("#authenticate");
    if (b.length) {
      b.on("submit", function(d) {
        d.preventDefault();
        d = jQuery("#inputPassword");
        var f = jQuery("#inputEmail");
        if(d && f){
          (new w).setMethod("POST").setData(b.serialize()).setUri(b.attr("action"))
          .setHandler(function(b){
            location.reload();
          }).send();
        }
      });
    }
    return this;
  };
  function loadGroup(element){
    this.element = jQuery(element);
    return this.run();
  }
  d = loadGroup.prototype;
  d.run = function(){
    var v = new w, self = this;
    v.setUri("/cp/xhr/groups?action=get").setMethod("POST").setHandler(function(b){
      b = JSON.parse(b);
      for(var i =0, il = b.length; i < il; ++i){
        var s = p("option").val(b[i].id).text(b[i].title).appendTo(self.element);
        if(b[i].id == self.element.attr("data-default")){
          s.attr({selected: "selected"});
        }
      }
    });
    v.send();
    return this;
  };
  n("loginActions", new E);
  n("mailActions", new y);
  n("groupActions", new C);
  n("scheduleActions", new z);
  n("userActions", new D);
  n("templateActions", new A);
  n("marketing", function() {
    var b = new x,
      d = jQuery("#mainContent");
    d.length && (d.each(u(b.mainContent, b, d)), jQuery(l).on("resize", u(b.mainContent, b, d)));
  });
})(window);