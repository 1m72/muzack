<?php
namespace App\Http\Controllers\Cp;

use App\Http\Controllers\CpController;
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;

class TemplatesController extends CpController{
  public function index(){

  // require_once app_path('Libraries/phpexcel/Classes/PHPExcel.php');
  // $excelFile = public_path('google.csv');

  // $objReader = \PHPExcel_IOFactory::createReader('CSV');
  // $objPHPExcel = $objReader->load($excelFile);

  // //Itrating through all the sheets in the excel workbook and storing the array data
  // foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
      // $arrayData[] = $worksheet->toArray();
  // }
  // $k = array();
  // $array_obj = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($arrayData));
  // foreach($array_obj as $key => $value) {
    // $value = trim($value);
    // if(strlen($value) && (strpos($value,'@') !== false)){
      // $k[] = $value;
    // }
  // }

  // _debug($k);
  // exit;
  if(\Request::isMethod('post')){
    $ids = \Request::input('ids');
    $ids = explode(',', $ids);
    \App\Models\Template::whereIn('id', $ids)->delete();
    \Session::flash('msg-success', count($ids).' records deleted.');
    return redirect(route('templates-index'));
  }
    $list = \App\Models\Template::orderBy('id','DESC')->get();
    return view('templates.index', array('list' => $list));
  }

  // public function create(){
    // if (\Request::isMethod('post')) {
      // $i = \Request::input();
      // $template = new \App\Models\Template;
      // $template->content = $i['content'];
      // $template->title = $i['title'];
      // $template->save();
      // \Session::flash('msg-success', 'Created.');
      // // echo \Session::get('msg-success');
      // // exit;
      // return redirect(route('templates-index'));
    // }
    // $template = new \stdClass;
    // return view('templates.edit', array('template' => $template));
  // }

  public function edit($id = ''){
    $template = \App\Models\Template::find($id);
    if(!$template){
      $template = new \App\Models\Template;
    }
    if (\Request::isMethod('post')) {
      $i = \Request::input();
      $template->content = $i['content'];
      $template->title = $i['title'];
      $template->save();
      \Session::flash('msg-success', config('marketing.item_saved'));
      return redirect(route('templates-index'));
    }
    return view('templates.edit', array('template' => $template));
  }
}