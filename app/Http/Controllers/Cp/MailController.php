<?php
namespace App\Http\Controllers\Cp;

use App\Http\Controllers\CpController;
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;

class MailController extends CpController {
  public function index() {
    if (\Request::isMethod('post')) {
      $i = \Request::input();
      if(!empty($i['action']) && ($i['action'] == 'true')){
        $filter = json_decode($i['filter'], true);
        return \Redirect::route('mail-index', $filter);
      }else{
        $ids = $i['ids'];
        $ids = explode(',', $ids);
        $emails = \App\Models\Email::whereIn('id', $ids)->get();
        foreach($emails as $email){
          $email->groups()->detach();
        }
        \App\Models\Email::whereIn('id', $ids)->delete();
        \Session::flash('msg-success', count($ids).' records deleted.');
        $filter = json_decode($i['filter'], true);
        return \Redirect::route('mail-index', $filter);
      }
    }
    $group_id = \Request::input('group_id');

    // \Event::listen('illuminate.query', function($query, $params, $time, $conn)
    // {
        // var_dump(array($query, $params, $time, $conn));
    // });

    // $list = \App\Models\Email::orderBy('id', 'DESC')->paginate(config('marketing.items_per_page'));

    if(!$group_id){
      $list = \App\Models\Email::orderBy('id', 'DESC')->paginate(config('marketing.items_per_page'));
    }else{
      // $followings = User::find(2)->followings()->with('userProfile')->select('users.*')->paginate(1);
      // $list = \App\Models\Group::with('emails')->where('id', '=', $group_id)->paginate(3);
      $list = \App\Models\Group::find($group_id)->emails()->with('groups')->paginate(config('marketing.items_per_page'));
    }
    // _debug($list);

    $groups = \App\Models\Group::all();
    return view('mail.index', array(
      'list' => $list,
      'filter' => \Request::input(),
      'groups' => $groups
    ));
  }

  public function import() {
    if (\Request::isMethod('post')) {
      $i = \Request::input();
      if (!empty($i['upload_id']) && !empty($i['group_id'])) {
        $upload_id = $i['upload_id'];
        $upload_id = base64_decode(\Session::get($upload_id));
        if (is_file($upload_id)) {
          if($this->fromFile($upload_id, $i['group_id'])){
            unlink($upload_id);
            \Session::flash('msg-success', 'Emails imported!');
            return redirect(route('mail-index'));
          }
        }
      } else {
        echo "There was an error uploading the file";
        exit;
        //return redirect(route('mail-import'));
      }
    }
    $groups = \App\Models\Group::all();
    return view('mail.import', array('groups' => $groups));
  }

  public function mimport(){
    if(\Request::isMethod('post')){
      $i = \Request::input();
      $job = new \App\Models\Job;
      $job->title = 'Import email via Imap on: '. $i['email'];
      $job->command = 'mail:imap';
      $job->content = json_encode($i);

      // if($this->_fromMail($i)){
      if($job->save()){
        \Session::flash('msg-success', 'Imported!');
        return redirect(route('mail-index'));
      }
      \Session::flash('msg-danger', 'Couldnt import email, please report to the developer.');
      return redirect(route('mail-index'));
    }
    $groups = \App\Models\Group::all();
    return view('mail.mimport', array(
      'site_title' => 'Import from email',
      'groups' => $groups
    ));
  }

  public function himport(){
    if (\Request::isMethod('post')) {
      $i = \Request::input();
      $exists = !!\App\Models\Email::where('address','=',$i['address'])->count();
      if(!$exists){
        $email = new \App\Models\Email;
        $email->address = $i['address'];
        $email->save();
        \Session::flash('msg-success','Saved.');
      }else{
        \Session::flash('msg-danger','This email is already existed.');
      }
      return redirect(route('mail-index'));
    }
    $groups = \App\Models\Group::all();
    return view('mail.himport', array('groups' => $groups));
  }

  public function fimport(){

    // Facebook app settings
    $app_id = '173014289530499';
    $app_secret = 'd4dfd64b9719ee35f172372f0eeecc78';
    $redirect_uri = route('mail-fimport');
    // Requested permissions for the app - optional
    $permissions = array(
      'email',
      'user_location',
      'user_birthday'
    );
    // Define the root directoy
    define( 'ROOT', dirname( __FILE__ ) . '/' );
    // Autoload the required files
    require_once( app_path('Libraries/facebook-php-sdk-v4-4.0-dev/autoload.php'));

    // $fb = new \Facebook\Facebook();

    // Initialize the SDK
    \Facebook\FacebookSession::setDefaultApplication( $app_id, $app_secret );
    // Create the login helper and replace REDIRECT_URI with your URL
    // Use the same domain you set for the apps 'App Domains'
    // e.g. $helper = new FacebookRedirectLoginHelper( 'http://mydomain.com/redirect' );
    $helper = new FacebookRedirectLoginHelper( $redirect_uri );
    // Check if existing session exists
    if ( isset( $_SESSION ) && isset( $_SESSION['fb_token'] ) ) {
      // Create new session from saved access_token
      $session = new FacebookSession( $_SESSION['fb_token'] );
        // Validate the access_token to make sure it's still valid
        try {
          if ( ! $session->validate() ) {
            $session = null;
          }
        } catch ( Exception $e ) {
          // Catch any exceptions
          $session = null;
        }
    } else {
      // No session exists
      try {
        $session = $helper->getSessionFromRedirect();
      } catch( FacebookRequestException $ex ) {
        // When Facebook returns an error
      } catch( Exception $ex ) {
        // When validation fails or other local issues
        echo $ex->message;
      }
    }
    // Check if a session exists
    if ( isset( $session ) ) {
      // Save the session
      $_SESSION['fb_token'] = $session->getToken();
      // Create session using saved token or the new one we generated at login
      $session = new FacebookSession( $session->getToken() );
      // Create the logout URL (logout page should destroy the session)
      $logoutURL = $helper->getLogoutUrl( $session, 'http://yourdomain.com/logout' );
      echo '<a href="' . $logoutURL . '">Log out</a>';

      $fb = new Facebook([
        'app_id' => $app_id,
        'app_secret' => $app_secret,
        'default_graph_version' => 'v2.2',
        ]);
      $res = $fb->get('/me');

      var_dump($res->getDecodedBody());
      exit;
    } else {
      // No session
      // Get login URL
      $loginUrl = $helper->getLoginUrl( $permissions );
      echo '<a href="' . $loginUrl . '">Log in</a>';
    }
  }

  public function fimportcb(){

  }

  private function _fromMail($input) {
    // 5 minutes
    set_time_limit(5*60);
    try{
      // $mailbox = new ImapMailbox('{lo1.infinitysrv.com:993/imap/ssl}INBOX', $i['email'], $i['password']);
      $imap = $this->_identity($input);

      $mailbox = new ImapMailbox('{'.$imap['host'].':'.$imap['port'].'/imap/'.$imap['secure'].'}INBOX', $input['email'], $input['password']);
      $mailsIds = $mailbox->searchMailbox('ALL');
      _debug($mailsIds);
      if (!$mailsIds) {
        \Log::error('Mailbox is empty.');
        return false;
      }
      $i        = 0;
      $contacts = array();
      foreach ($mailsIds as $id) {
        // $mailId = reset($mailsIds);
        // should be limited on local development
        if (++$i == 8 && ('local' == \App::environment())) {
          break;
        }
        $mail       = $mailbox->getMail($id);
        $contacts[] = trim($mail->fromAddress);
      }

      $contacts = array_unique($contacts);
      return $this->_import($contacts, $input['group_id']);
    }catch (\Exception $e){
      \Log::error($e);
      return false;
    }
    return false;
  }

  public function send() {
    if (\Request::isMethod('post')) {
      $params                = array();
      $i                     = \Request::input();
      $params['email']    = $i['email'];
      $params['password']    = $i['password'];
      $params['template_id'] = $i['template_id'];

      if (!$params['email'] || !$params['password'] || !$params['template_id']) {
        exit('email and password are required');
      }

      $imap = $this->identity($params['email']);

      $emails = \App\Models\Email::all();
      $mail   = new \PHPMailer;
      $mail->isSMTP();
      $mail->Host       = $imap['host'];
      $mail->SMTPAuth   = true;
      $mail->Username   = $params['email'];
      $mail->Password   = $params['password'];
      $mail->SMTPSecure = 'ssl';
      $mail->isHTML(true);
      $mail->Port = $imap['port'];

      $template = \App\Template::find($params['template_id']);
      // _debug($template->content);

      $mail->Subject = 'Here is the subject';
      //$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
      $mail->Body    = $template->content;
      $mail->setFrom($params['email'], 'Nguyen Thanh');

      // foreach ($emails as $email) {
        // $mail->addBcc($email->address);
      // }
      $mail->addAddress('nguyenthanh.php@gmail.com');

      if (!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
        exit();
      } else {
        //echo 'Message has been sent';
        \Session::flash('msg-success','Mail has been sent!');
        return redirect(route('mail-index'));
      }
    }
    $templates = \App\Template::all();
    return view('mail.send', array(
      'templates' => $templates
    ));

    // \Mail::raw('Text to e-mail', function($message) use ($toSend) {
    // // $message->from("1234");
    // $message->to($toSend);
    // });
  }

  private function _import($l, $group_id) {
    if (!$l || !$group_id) {
      return false;
    }

    try {
      $eids = array();
      foreach ($l as $e) {
        $m = \App\Models\Email::firstOrCreate(array('address' => $e));
        $eids[] = $m->id;
      }
      $group = \App\Models\Group::find($group_id);
      $group->emails()->sync($eids, false);

      return true;
    }
    catch (Exception $e) {
      return false;
    }
  }

  private function fromFile($file, $group_id) {
    set_time_limit(0);
    $file_extension = file_extension($file);

    if('txt' == $file_extension){
      $l = file_get_contents($file);
      if (!$l) {
        return false;
      }
      $l = explode(PHP_EOL, $l);
      $l = array_filter(array_map('trim', $l), 'strlen');
      $l = array_values($l);
      return $this->_import($l, $group_id);
    }

    if('csv' == $file_extension){
      $l = $this->_fromCsv($file);
      return $this->_import($l, $group_id);
    }

    return false;
  }

  private function identity($email){
    $domain = substr(strrchr($email, "@"), 1);
    if('codelovers.vn' == $domain){
      $domain = array(
        'host' => 'lo1.infinitysrv.com',
        'port' => 465
      );
      return $domain;
    }
    return null;
  }

  public function group($group_id){
    $list = \App\Models\Group::with('emails')->where('id', '=', $group_id)->get();
    return view('mail.group', array(
      'list' => $list
    ));
  }

  protected function _identity($i){
    $imap = false;
    $domain = substr(strrchr($i['email'], '@'), 1);
    switch($domain){
      case 'codelovers.vn':
        $imap = array(
          'host' => 'lo1.infinitysrv.com',
          'port' => 993,
          'secure' => 'ssl'
        );
        break;
      case 'gmail.com':
        $imap = array(
          'host' => 'imap.gmail.com',
          'port' => 993,
          'secure' => 'ssl'
        );
        break;
      case 'yahoo.com':
      case 'yahoo.com.vn':
        $imap = array(
          'host' => 'imap.mail.yahoo.com',
          'port' => 993,
          'secure' => 'ssl'
        );
        break;
    }
    return $imap;

    // $mailbox = new ImapMailbox('{lo1.infinitysrv.com:993/imap/ssl}INBOX', $i['email'], $i['password']);
  }

  protected function _fromCsv($file){
    require_once app_path('Libraries/phpexcel/Classes/PHPExcel.php');
    $objReader = \PHPExcel_IOFactory::createReader('CSV');
    $objPHPExcel = $objReader->load($file);

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
        $arrayData[] = $worksheet->toArray();
    }
    $k = array();
    $array_obj = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($arrayData));
    foreach($array_obj as $key => $value) {
      $value = trim($value);
      if(strlen($value) && (strpos($value,'@') !== false)){
        $k[] = $value;
      }
    }

    // remove duplicates emails!
    $k = array_unique($k);

    return $k;
  }
}
