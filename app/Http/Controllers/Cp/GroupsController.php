<?php
namespace App\Http\Controllers\Cp;
use App\Http\Controllers\CpController;

class GroupsController extends CpController{
  public function index(){
    if(\Request::isMethod('post')){
      $ids = \Request::input('ids');
      $ids = explode(',', $ids);
      $groups = \App\Models\Group::whereIn('id', $ids)->get();
      foreach($groups as $group){
        $group->emails()->detach();
      }

      \App\Models\Group::whereIn('id', $ids)->delete();
      \Session::flash('msg-success', count($ids).' records deleted.');
      return redirect(route('groups-index'));
    }
    $list = \App\Models\Group::orderBy('id','DESC')->get();
    return view('groups.index', array('list' => $list));
  }

  // public function create(){
    // if (\Request::isMethod('post')) {
      // $i = \Request::input();
      // $group = new \App\Models\Group;
      // $group->title = $i['title'];
      // $group->save();
      // \Session::flash('msg-success', 'Created.');
      // return redirect(route('groups-index'));
    // }
    // $group = array();
    // return view('groups.edit', array('group' => $group));
  // }

  public function edit($id = ''){
    $group = \App\Models\Group::find($id);
    if(!$group){
      $group = new \App\Models\Group;
    }
    if (\Request::isMethod('post')) {
      $i = \Request::input();
      $group->title = $i['title'];
      $group->save();
      \Session::flash('msg-success', config('marketing.item_saved'));
      return redirect(route('groups-index'));
    }
    return view('groups.edit', array('group' => $group));
  }
}
