<?php
namespace App\Http\Controllers\Cp;
use App\Http\Controllers\CpController;
class XhrController extends CpController {
  public function groups() {
    $i = \Request::input();
    $action = $i['action'];
    switch ($action){
      case 'get':
        $groups = \App\Models\Group::all();
        return response()->json($groups);
      case 'new':
        $group = new \App\Models\Group;
        $group->title = $i['title'];
        $group->save();
        return response()->json($group);
      case 'autocomplete':
        $keyword = $i['keyword'];
        $groups = \App\Models\Group::where('title', 'LIKE', "%{$keyword}%")->get();
        return response()->json(array('items' => $groups->toArray()));
    }
  }
  public function email() {
    $action = \Request::input('action');
    if ('to_group' == $action) {
      $i = \Request::input();
      if (count($i['emails'])) {
        $group = \App\Models\Group::find($i['group_id']);
        $group->emails()->sync($i['emails'], false);
      }
      return response()->json(array(
        'success' => true
      ));
    }
  }
  public function upload() {
    $shortcut = 'media';
    $upload   = $this->doUpload(public_path($shortcut));
    $response = array();
    $status   = false;
    #_debug($upload);
    if ($upload) {
      $status = true;
      // $response['file_url'] = $shortcut.$upload['sanitize_name'];
    }
    $response['status'] = $status;
    $response['file']   = $upload['file'];
    return response()->json($response);
  }
  private function doUpload($targetDir) {
    // 5 minutes execution time
    set_time_limit(5 * 60);
    try {
      $return           = array();
      // Uncomment this one to fake upload time
      // usleep(5000);
      // Settings
      // $targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
      // $targetDir = 'uploads';
      $cleanupTargetDir = true; // Remove old files
      $maxFileAge       = 5 * 3600; // Temp file age in seconds
      // Create target dir
      if (!file_exists($targetDir)) {
        mkdir($targetDir, 0777, true);
      }
      // Get a file name
      if (isset($_REQUEST["name"])) {
        $fileName = $_REQUEST["name"];
      } elseif (!empty($_FILES)) {
        $fileName = $_FILES["file"]["name"];
      } else {
        $fileName = uniqid("file_");
      }
      $file_ext                 = file_extension($fileName);
      $file_name_part           = substr($fileName, 0, strrpos($fileName, "."));
      $return['real_name']      = $file_name_part;
      $return['file_extension'] = $file_ext;
      $return['full_real_name'] = $fileName;
      $file_name_part           = sanitize($file_name_part);
      $fileName                 = $file_name_part . '.' . $file_ext;
      $return['sanitize_name']  = $fileName;
      $filePath                 = $targetDir . DIRECTORY_SEPARATOR . $fileName;
      $return['file_path']      = $filePath;
      // Chunking might be enabled
      $chunk                    = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
      $chunks                   = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
      // Remove old temp files
      if ($cleanupTargetDir) {
        if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
          throw new \Exception('Failed to open temp directory');
          #die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
          #return false;
        }
        while (($file = readdir($dir)) !== false) {
          $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
          // If temp file is current file proceed to the next
          if ($tmpfilePath == "{$filePath}.part") {
            continue;
          }
          // Remove temp file if it is older than the max age and is not the current file
          if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
            @unlink($tmpfilePath);
          }
        }
        closedir($dir);
      }
      // Open temp file
      if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
        throw new \Exception('Failed to open output stream');
        #die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        #return false;
      }
      if (!empty($_FILES)) {
        if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
          throw new \Exception('Failed to move uploaded file');
          #die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
          #return false;
        }
        // Read binary input stream and append it to temp file
        if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
          throw new \Exception('Failed to open input stream');
          #die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
          #return false;
        }
      } else {
        if (!$in = @fopen("php://input", "rb")) {
          throw new \Exception('Failed to open input stream');
          #die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
          #return false;
        }
      }
      while ($buff = fread($in, 4096)) {
        fwrite($out, $buff);
      }
      @fclose($out);
      @fclose($in);
      // Check if file has been uploaded
      if (!$chunks || $chunk == $chunks - 1) {
        // Strip the temp .part suffix off
        rename("{$filePath}.part", $filePath);
      }
      $hash_key = generate_random_string(40);
      \Session::put($hash_key, base64_encode($filePath));
      $return['file'] = $hash_key;
      return $return;
    }
    catch (\Exception $e) {
      return $e->__toString();
    }
    return false;
  }
}
