<?php
namespace App\Http\Controllers\Cp;
use App\Http\Controllers\CpController;

class ScheduleController extends CpController{
  public function index(){
    if(\Request::isMethod('post')){
      $ids = \Request::input('ids');
      $ids = explode(',', $ids);
      \App\Models\Schedule::whereIn('id', $ids)->delete();
      \Session::flash('msg-success', count($ids).' records deleted.');
      return redirect(route('schedule-index'));
    }
    $list = \App\Models\Schedule::orderBy('id','DESC')->paginate(config('marketing.items_per_page'));
    return view('schedule.index', array('list' => $list));
  }

  // public function create(){
    // if (\Request::isMethod('post')) {
      // $i = \Request::input();
      // $schedule = new \App\Models\Schedule;
      // $schedule->title = $i['title'];
      // $schedule->group_id = $i['group_id'];
      // $schedule->template_id = $i['template_id'];
      // $schedule->user_id = $i['user_id'];
      // $schedule->start_time = strtotime($i['start_time']);
      // $schedule->save();
      // \Session::flash('msg-success', 'Schedule has been created.');
      // return redirect(route('schedule-index'));
    // }
    // $schedule = array();
    // $groups = \App\Models\Group::all();
    // $templates = \App\Models\Template::all();
    // $users = \App\Models\User::all();
    // return view('schedule.edit', array('schedule' => $schedule,
      // 'templates' => $templates,
      // 'groups' => $groups,
      // 'users' => $users
    // ));
  // }

  public function edit($id = ''){
    $schedule = \App\Models\Schedule::find($id);
    if(!$schedule){
      $schedule = new \App\Models\Schedule;
    }
    if (\Request::isMethod('post')) {
      if($schedule->finished){
        \Session::flash('msg-danger', 'The task is completed.');
        return redirect(route('schedule-index'));
      }
      $i = \Request::input();
      $schedule->title = $i['title'];
      $schedule->group_id = $i['group_id'];
      $schedule->template_id = $i['template_id'];
      $schedule->user_id = $i['user_id'];
      $schedule->start_time = strtotime($i['start_time']);
      $schedule->save();
      \Session::flash('msg-success', 'Saved.');
      return redirect(route('schedule-index'));
    }
    // $groups = \App\Models\Group::find($schedule->group_id);
    $groups = \App\Models\Group::all();
    $templates = \App\Models\Template::all();
    $users = \App\Models\User::all();
    return view('schedule.edit', array('schedule' => $schedule,
      'templates' => $templates,
      'groups' => $groups,
      'users' => $users
    ));
  }
}