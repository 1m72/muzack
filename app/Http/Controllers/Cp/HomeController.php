<?php
namespace App\Http\Controllers\Cp;
use App\Http\Controllers\CpController;

class HomeController extends CpController{
  public function index(){
    return view('home.index');
  }

  public function login(){
    // \Session::forget('auth-am');
    // _debug(session('auth-am'));
    // _debug(\Session::get('auth-am'));
    if(\Session::has('auth-am')){
      $return = \Session::get('auth-return');
      if($return){
        \Session::forget('auth-return');
        return redirect(route($return));
      }
      return redirect(route('templates-index'));
    }
    if(\Request::isMethod('post')){
      $i = \Request::input();
      $u = \App\Models\Administrator::where('email','=', $i['email'])->where('password', '=', $i['password'])->first();
      if($u){
        \Session::put('auth-am', $u);
        return response()->json(array('success' => true));
      }
      return response()->json(array('success' => false));
    }

    return view('home.login', array('site_title' => 'Log in'));
  }

  public function logout(){
    \Session::forget('auth-am');
    return redirect(route('home-login'));
  }
}
