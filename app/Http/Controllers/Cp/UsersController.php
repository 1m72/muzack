<?php
namespace App\Http\Controllers\Cp;
use App\Http\Controllers\CpController;
class UsersController extends CpController {

  public function __construct(){
    // require_once app_path('Libraries/bcrypt/Bcrypt.php');
  }

  public function index(){
    if(\Request::isMethod('post')){
      $ids = \Request::input('ids');
      $ids = explode(',', $ids);
      \App\Models\User::whereIn('id', $ids)->delete();
      \Session::flash('msg-success', count($ids).' records deleted.');
      return redirect(route('users-index'));
    }
    $list = \App\Models\User::all();
    return view('users.index', array('list' => $list));
  }

  public function edit($id){
    $user = \App\Models\User::find($id);
    if (\Request::isMethod('post')) {
      $i = \Request::input();
      if(!empty($i['password'])){
        $user->password = \App\Libraries\Encryption::encode($i['password']);
      }
      $user->email = $i['email'];
      $user->username = $i['username'];
      $user->save();
      \Session::flash('msg-success', 'Record updated.');
      return redirect(route('users-index'));
    }
    return view('users.edit', array('user' => $user));
  }

  public function create(){
    if (\Request::isMethod('post')) {
      $i = \Request::input();
      $user = new \App\Models\User;
      $user->email = $i['email'];
      $user->password = \App\Libraries\Encryption::encode($i['password']);
      $user->username = $i['username'];
      $user->save();

      \Session::flash('msg-success', 'Created.');
      return redirect(route('users-index'));
    }
    $user = array();
    return view('users.create', array('user' => $user));
  }
}