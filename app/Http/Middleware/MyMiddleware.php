<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class MyMiddleware{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

  public function handle($request, Closure $next)
	{
		if(!\Session::get('auth-am') &&('home-login' != \Route::getRoutes()->match($request)->getName())){
      \Session::put('auth-return', \Route::getRoutes()->match($request)->getName());
      return redirect()->route('home-login');
    }
		return $next($request);
	}

}