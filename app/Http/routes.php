<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Cp', 'prefix' => 'cp','after'=>'no-cache'], function(){

  Route::match(['get', 'post'], 'home/index', ['as' => 'home-index', 'uses' => 'HomeController@index']);
  Route::match(['get', 'post'], 'home/login', ['as' => 'home-login', 'uses' => 'HomeController@login']);
  Route::get('home/logout', ['as' => 'home-logout', 'uses' => 'HomeController@logout']);

  Route::match(['get', 'post'], 'templates', ['as' => 'templates-index', 'uses' => 'TemplatesController@index']);
  Route::match(['get', 'post'], 'templates/create', ['as' => 'templates-create', 'uses' => 'TemplatesController@create']);
  Route::match(['get', 'post'], 'templates/edit/{id?}', ['as' => 'templates-edit', 'uses' => 'TemplatesController@edit']);

  Route::match(['get', 'post'], 'schedule', ['as' => 'schedule-index', 'uses' => 'ScheduleController@index']);
  Route::match(['get', 'post'], 'schedule/create', ['as' => 'schedule-create', 'uses' => 'ScheduleController@create']);
  Route::match(['get', 'post'], 'schedule/edit/{id?}', ['as' => 'schedule-edit', 'uses' => 'ScheduleController@edit']);

  Route::match(['get','post'], 'mail', ['as' => 'mail-index', 'uses' => 'MailController@index']);
  Route::match(['get','post'], 'mail/import', ['as' => 'mail-import', 'uses' => 'MailController@import']);
  Route::match(['get','post'], 'mail/group/{id}', ['as' => 'mail-group', 'uses' => 'MailController@group']);

  Route::match(['get','post'], 'mail/himport', ['as' => 'mail-himport', 'uses' => 'MailController@himport']);
  Route::match(['get','post'], 'mail/mimport', ['as' => 'mail-mimport', 'uses' => 'MailController@mimport']);
  Route::match(['get','post'], 'mail/fimport', ['as' => 'mail-fimport', 'uses' => 'MailController@fimport']);
  Route::match(['get','post'], 'mail/fimportcb', ['as' => 'mail-fimportcb', 'uses' => 'MailController@fimportcb']);

  Route::match(['get','post'], 'groups', ['as' => 'groups-index', 'uses' => 'GroupsController@index']);
  Route::match(['get', 'post'], 'groups/create', ['as' => 'groups-create', 'uses' => 'GroupsController@create']);
  Route::match(['get', 'post'], 'groups/edit/{id?}', ['as' => 'groups-edit', 'uses' => 'GroupsController@edit']);

  Route::match(['get','post'], 'users', ['as' => 'users-index', 'uses' => 'UsersController@index']);
  Route::match(['get', 'post'], 'users/create', ['as' => 'users-create', 'uses' => 'UsersController@create']);
  Route::match(['get', 'post'], 'users/edit/{id}', ['as' => 'users-edit', 'uses' => 'UsersController@edit']);

  Route::match(['get','post'], 'mail/create', ['as' => 'mail-create', 'uses' => 'MailController@create']);
  Route::match(['get','post'], 'mail/send', ['as' => 'mail-send', 'uses' => 'MailController@send']);
  Route::match(['get','post'], 'mail/address', ['as' => 'mail-address', 'uses' => 'MailController@address']);


  Route::post('xhr/upload', ['as' => 'xhr-upload', 'uses' => 'XhrController@upload']);
  Route::match(['post', 'get'], 'xhr/groups', ['as' => 'xhr-groups', 'uses' => 'XhrController@groups']);
  Route::post('xhr/email', ['as' => 'xhr-email', 'uses' => 'XhrController@email']);
});