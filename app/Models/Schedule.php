<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'start_time', 'group_id', 'template_id'
    ];

    protected $table = 'schedule';

    public function group()
    {
        return $this->belongsTo('\App\Models\Group');
    }

}
