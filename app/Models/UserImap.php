<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserImap extends Model
{
  protected $table = 'users_imap';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'user_id', 'secure', 'port', 'url'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
