<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address'
    ];

    public function groups() // or payments() ?
    {
       return $this->belongsToMany('\App\Models\Group')->withTimestamps();
    }

    public static function import($l, $group_id) {
      if (!$l || !$group_id) {
        return false;
      }

      try {
        $eids = array();
        foreach ($l as $e) {
          $m = self::firstOrCreate(array('address' => $e));
          $eids[] = $m->id;
        }
        $group = \App\Models\Group::find($group_id);
        $group->emails()->sync($eids, false);

        return true;
      }
      catch (Exception $e) {
        return false;
      }
      return false;
    }

    public static function identity($email) {
      $imap   = false;
      $domain = substr(strrchr($email, '@'), 1);
      switch ($domain) {
        case 'codelovers.vn':
          $imap = array(
            'host' => 'lo1.infinitysrv.com',
            'port' => 993,
            'secure' => 'ssl'
          );
          break;
        case 'gmail.com':
          $imap = array(
            'host' => 'imap.gmail.com',
            'port' => 993,
            'secure' => 'ssl'
          );
          break;
        case 'yahoo.com':
        case 'yahoo.com.vn':
          $imap = array(
            'host' => 'imap.mail.yahoo.com',
            'port' => 993,
            'secure' => 'ssl'
          );
          break;
      }
      return $imap;

      // $mailbox = new ImapMailbox('{lo1.infinitysrv.com:993/imap/ssl}INBOX', $i['email'], $i['password']);
    }
}
