<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;

class MailImap extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'mail:imap';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = '';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle() {
    // execute action in 3 hours
    set_time_limit(3 * 60 * 60);

    $tasks = \App\Models\Job::where(array(
        'command' => 'mail:imap',
        'finished' => false
    ))->get();
    $bar = $this->output->createProgressBar(count($tasks));

    foreach ($tasks as $task) {
      $task->finished = true;
      $task->save();

      $content = json_decode($task->content, true);
      $imap    = \App\Models\Email::identity($content['email']);

      $mailbox  = new ImapMailbox('{' . $imap['host'] . ':' . $imap['port'] . '/imap/' . $imap['secure'] . '}INBOX', $content['email'], $content['password']);
      $mailsIds = $mailbox->searchMailbox('ALL');

      if (!$mailsIds) {
        \Log::error('Mailbox is empty.');
        return false;
      }
      $i        = 0;
      $contacts = array();
      foreach ($mailsIds as $id) {
        // $mailId = reset($mailsIds);
        // should be limited on local development
        if (++$i == 8 && ('local' == \App::environment())) {
          break;
        }
        $mail       = $mailbox->getMail($id);
        $contacts[] = trim($mail->fromAddress);
      }

      $contacts = array_unique($contacts);
      \App\Models\Email::import($contacts, $content['group_id']);
      // $this->info(count($contacts). ' contacts has been imported!');
      $bar->advance();
    }
    $bar->finish();
  }
}
