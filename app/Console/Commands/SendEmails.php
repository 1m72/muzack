<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendEmails extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'emails:send';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Send mail as scheduled';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct() {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle() {
    // 1 hour
    set_time_limit(60*60);

    $schedules = \App\Models\Schedule::all();
    foreach ($schedules as $schedule) {
      // \Log::info(!!$schedule->finished ? "yes" : "no");
      // \Log::info($schedule->execution_time);
      // \Log::info($schedule->start_time < time() ? "yes" : "no");
      // \Log::info(date('Y-m-d H:i', $schedule->start_time));
      // \Log::info(date('Y-m-d H:i', time()));
      if ((!$schedule->finished) && (!$schedule->execution_time) && ($schedule->start_time <= time())) {
        $this->send($schedule);
        return '';
      }
    }
  }

  protected function send($schedule) {
    $group = \App\Models\Group::find($schedule->group_id);
    if(!$group){
      throw new \Exception('Group not found!');
    }
    $emails = $group->emails;
    if(!$emails){
      throw new \Exception('No email found!');
    }
    // _debug(count($emails));
    if (!empty($emails)) {
      // $imap = array(
        // 'port' => intval(env('MAIL_PORT')),
        // 'host' => env('MAIL_HOST'),
        // 'email' => env('MAIL_USERNAME'),
        // 'password' => env('MAIL_PASSWORD'),
        // 'secure' => env('MAIL_ENCRYPTION')
      // );

      $imap = $this->_identity($schedule);
      // _debug($imap);
      if(!$imap){
        throw new \Exception('Imap for this user not found!');
      }

      $mail = new \PHPMailer;
      $mail->isSMTP();
      // $mail->SMTPDebug  = 3;
      $mail->SMTPAuth   = true;
      $mail->Host       = $imap['host'];
      $mail->Username   = $imap['email'];
      $mail->Password   = $imap['password'];
      $mail->SMTPSecure = $imap['secure'];
      $mail->isHTML(true);
      $mail->Port = $imap['port'];

      $template = \App\Models\Template::find($schedule['template_id']);
      $this->fixMailBody($template);
      $mail->Subject = 'Email Marketing';
      //$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
      $mail->Body    = $template;
      $mail->setFrom($imap['email'], $imap['username']);

      foreach ($emails as $email) {
        $mail->addBcc($email->address);
      }
      // $mail->addAddress('nguyenthanh.php@gmail.com');

      if (!$mail->send()) {
        // echo 'Message could not be sent.';
        // echo 'Mailer Error: ' . $mail->ErrorInfo;
        // exit();
        \Log::info('Mailer Error: ' . $mail->ErrorInfo);
        return false;
      } else {
        $schedule->execution_time = time();
        $schedule->finished       = true;
        $schedule->save();
        \Log::info('Schedule executed at:' . date('Y-m-d H:i:s'));
        // $schedule->delete();
        return true;
      }
    }
  }

  private function fixMailBody(&$template){
    $template = trim($template->content);
    $template = preg_replace( "/\r|\n/", "", $template);
    if('.' != substr($template, -1)){
      $template .= '.';
    }
  }

  protected function _identity($schedule){
    $user = \App\Models\User::find($schedule->user_id);
    if(!$user){
      return false;
    }
    $imap = false;
    $domain = substr(strrchr($user->email, '@'), 1);

    switch($domain){
      case 'codelovers.vn':
        $imap = array(
          'host' => 'lo1.infinitysrv.com',
          'port' => 465,
          'secure' => 'ssl'
        );
        break;
      case 'gmail.com':
        $imap = array(
          'host' => 'smtp.gmail.com',
          'port' => 465,
          'secure' => 'ssl'
        );
        break;
      case 'yahoo.com':
      case 'yahoo.com.vn':
        $imap = array(
          'host' => 'smtp.mail.yahoo.com',
          'port' => 465,
          'secure' => 'ssl'
        );
        break;
    }

    if($imap){
      $imap['username'] = ($user->username ? $user->username : 'Marketing');
      $imap['password'] = \App\Libraries\Encryption::decode($user->password);
      $imap['email'] = $user->email;
      return $imap;
    }
    return false;
  }
}
